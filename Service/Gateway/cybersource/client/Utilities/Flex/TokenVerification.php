<?php

namespace UnboundCommerce\GooglePay\Service\Gateway\cybersource\client\Utilities\Flex;
use UnboundCommerce\GooglePay\Logger\Logger;

class TokenVerification
{
	/**
	 * @var Logger
	 */
	protected $logger;

	/**
	 * Sets logger
	 *
	 * @param Logger $logger
	 */
	public function setLogger($logger)
	{
		$this->logger = $logger;
	}


	public function verifyToken($publicKey, $postParam)
	{
		$dataString = "";
		$arraySting = explode(",", $postParam[0]['signedFields']);
		$lastElement = end($arraySting);
		$postParam = json_decode($postParam[0]);
		foreach ($arraySting as $value) {
			$dataString .= $postParam->$value;
			if($lastElement != $value){
				$dataString .= ",";
			}

		}
		$signature = base64_decode($postParam->signature);
		$signatureVerify = openssl_verify($dataString, $signature, $publicKey, "sha512");
		if ($signatureVerify == 1) {
			return "true";
		} elseif ($signatureVerify == 0) {
			return "false";
		} else {
			$this->logger->addError("Error in checking signature\n");
		}
	}
}
